package org.databandtech.flinkstreaming.Entity;

public class ItemCount {
	
	String type;
	String mainKey;
	String subKey;
	Long windowEnd;
	Long count;

	
	public ItemCount(String type, String key1, String key2, Long windowEnd, Long count) {
		this.type = type;
		this.mainKey = key1;
		this.subKey = key2;
		this.windowEnd =windowEnd;
		this.count =count;
	}
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	public String getMainKey() {
		return mainKey;
	}
	public void setMainKey(String mainKey) {
		this.mainKey = mainKey;
	}
	public String getSubKey() {
		return subKey;
	}
	public void setSubKey(String subKey) {
		this.subKey = subKey;
	}
	public Long getWindowEnd() {
		return windowEnd;
	}
	public void setWindowEnd(Long windowEnd) {
		this.windowEnd = windowEnd;
	}
	public Long getCount() {
		return count;
	}
	public void setCount(Long count) {
		this.count = count;
	}
	@Override
	public String toString() {
		return "ItemCount [type=" + type + ", mainKey=" + mainKey + ", subKey=" + subKey + ", windowEnd=" + windowEnd
				+ ", count=" + count + "]";
	}

}
