package com.ruoyi.web.controller.databand;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.web.domain.DatabandReport;
import com.ruoyi.web.service.IDatabandReportService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 报Controller
 * 
 * @author databand
 * @date 2020-12-31
 */
@Controller
@RequestMapping("/web/report")
public class DatabandReportController extends BaseController
{
    private String prefix = "web/report";

    @Autowired
    private IDatabandReportService databandReportService;

    @RequiresPermissions("web:report:view")
    @GetMapping()
    public String report()
    {
        return prefix + "/report";
    }

    /**
     * 查询报列表
     */
    @RequiresPermissions("web:report:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(DatabandReport databandReport)
    {
        startPage();
        List<DatabandReport> list = databandReportService.selectDatabandReportList(databandReport);
        return getDataTable(list);
    }

    /**
     * 导出报列表
     */
    @RequiresPermissions("web:report:export")
    @Log(title = "报", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(DatabandReport databandReport)
    {
        List<DatabandReport> list = databandReportService.selectDatabandReportList(databandReport);
        ExcelUtil<DatabandReport> util = new ExcelUtil<DatabandReport>(DatabandReport.class);
        return util.exportExcel(list, "report");
    }

    /**
     * 新增报
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存报
     */
    @RequiresPermissions("web:report:add")
    @Log(title = "报", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(DatabandReport databandReport)
    {
        return toAjax(databandReportService.insertDatabandReport(databandReport));
    }

    /**
     * 修改报
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        DatabandReport databandReport = databandReportService.selectDatabandReportById(id);
        mmap.put("databandReport", databandReport);
        return prefix + "/edit";
    }

    /**
     * 修改保存报
     */
    @RequiresPermissions("web:report:edit")
    @Log(title = "报", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(DatabandReport databandReport)
    {
        return toAjax(databandReportService.updateDatabandReport(databandReport));
    }

    /**
     * 删除报
     */
    @RequiresPermissions("web:report:remove")
    @Log(title = "报", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(databandReportService.deleteDatabandReportByIds(ids));
    }
}
