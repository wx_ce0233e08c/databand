package com.ruoyi.web.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.web.mapper.DatabandSitemenuMapper;
import com.ruoyi.web.domain.DatabandSitemenu;
import com.ruoyi.web.service.IDatabandSitemenuService;
import com.ruoyi.common.core.text.Convert;

/**
 * 站点菜单Service业务层处理
 * 
 * @author databand
 * @date 2020-12-31
 */
@Service
public class DatabandSitemenuServiceImpl implements IDatabandSitemenuService 
{
    @Autowired
    private DatabandSitemenuMapper databandSitemenuMapper;

    /**
     * 查询站点菜单
     * 
     * @param id 站点菜单ID
     * @return 站点菜单
     */
    @Override
    public DatabandSitemenu selectDatabandSitemenuById(Long id)
    {
        return databandSitemenuMapper.selectDatabandSitemenuById(id);
    }

    /**
     * 查询站点菜单列表
     * 
     * @param databandSitemenu 站点菜单
     * @return 站点菜单
     */
    @Override
    public List<DatabandSitemenu> selectDatabandSitemenuList(DatabandSitemenu databandSitemenu)
    {
        return databandSitemenuMapper.selectDatabandSitemenuList(databandSitemenu);
    }

    /**
     * 新增站点菜单
     * 
     * @param databandSitemenu 站点菜单
     * @return 结果
     */
    @Override
    public int insertDatabandSitemenu(DatabandSitemenu databandSitemenu)
    {
        return databandSitemenuMapper.insertDatabandSitemenu(databandSitemenu);
    }

    /**
     * 修改站点菜单
     * 
     * @param databandSitemenu 站点菜单
     * @return 结果
     */
    @Override
    public int updateDatabandSitemenu(DatabandSitemenu databandSitemenu)
    {
        return databandSitemenuMapper.updateDatabandSitemenu(databandSitemenu);
    }

    /**
     * 删除站点菜单对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteDatabandSitemenuByIds(String ids)
    {
        return databandSitemenuMapper.deleteDatabandSitemenuByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除站点菜单信息
     * 
     * @param id 站点菜单ID
     * @return 结果
     */
    @Override
    public int deleteDatabandSitemenuById(Long id)
    {
        return databandSitemenuMapper.deleteDatabandSitemenuById(id);
    }
}
