package org.databandtech.job.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.databandtech.job.entity.ScheduledBean;

@Mapper
public interface ScheduledMapper {
	
	@Select("select id,jobcode,jobtype,beaname,methodname,methodparams,cron,descri,ext,status,startflag,create_time,update_time from databand_scheduletask where jobcode = '${jobcode}' ")
    ScheduledBean getByCode(@Param("jobcode") String jobcode);
	
	@Select("select id,jobcode,jobtype,beaname,methodname,methodparams,cron,descri,ext,status,startflag,create_time,update_time from databand_scheduletask ")
    List<ScheduledBean> getAll();

	@Select("select id,jobcode,jobtype,beaname,methodname,methodparams,cron,descri,ext,status,startflag,create_time,update_time from databand_scheduletask where status=1")
    List<ScheduledBean> getAllFinished();

	@Select("select id,jobcode,jobtype,beaname,methodname,methodparams,cron,descri,ext,status,startflag,create_time,update_time from databand_scheduletask where startflag=0")
    List<ScheduledBean> getAllReadyStart();

}
