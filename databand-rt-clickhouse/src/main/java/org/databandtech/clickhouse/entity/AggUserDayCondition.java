package org.databandtech.clickhouse.entity;

public class AggUserDayCondition {
	
	String source;
	String datekey;
	String pt;
	
	public AggUserDayCondition(String source, String datekey, String pt) {
		super();
		this.source = source;
		this.datekey = datekey;
		this.pt = pt;
	}
	
	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}
	public String getDatekey() {
		return datekey;
	}
	public void setDatekey(String datekey) {
		this.datekey = datekey;
	}
	public String getPt() {
		return pt;
	}
	public void setPt(String pt) {
		this.pt = pt;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((datekey == null) ? 0 : datekey.hashCode());
		result = prime * result + ((pt == null) ? 0 : pt.hashCode());
		result = prime * result + ((source == null) ? 0 : source.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AggUserDayCondition other = (AggUserDayCondition) obj;
		if (datekey == null) {
			if (other.datekey != null)
				return false;
		} else if (!datekey.equals(other.datekey))
			return false;
		if (pt == null) {
			if (other.pt != null)
				return false;
		} else if (!pt.equals(other.pt))
			return false;
		if (source == null) {
			if (other.source != null)
				return false;
		} else if (!source.equals(other.source))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "AggUserDayCondition [source=" + source + ", datekey=" + datekey + ", pt=" + pt + "]";
	}

}
