package org.databandtech.api.service;

import java.util.List;

import org.databandtech.api.entity.DatabandSitemenu;


/**
 * 站点菜单Service接口
 * 
 * @author databand
 * @date 2020-12-31
 */
public interface IDatabandSitemenuService 
{
    /**
     * 查询站点菜单
     * 
     * @param id 站点菜单ID
     * @return 站点菜单
     */
    public DatabandSitemenu selectDatabandSitemenuById(Long id);

    /**
     * 查询站点菜单列表
     * 
     * @param databandSitemenu 站点菜单
     * @return 站点菜单集合
     */
    public List<DatabandSitemenu> selectDatabandSitemenuList(DatabandSitemenu databandSitemenu);
    public List<DatabandSitemenu> selectDatabandSitemenuListBySiteid(Long siteid);

    /**
     * 新增站点菜单
     * 
     * @param databandSitemenu 站点菜单
     * @return 结果
     */
    public int insertDatabandSitemenu(DatabandSitemenu databandSitemenu);

    /**
     * 修改站点菜单
     * 
     * @param databandSitemenu 站点菜单
     * @return 结果
     */
    public int updateDatabandSitemenu(DatabandSitemenu databandSitemenu);


}
