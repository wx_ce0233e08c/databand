package org.databandtech.mockapi;

import static org.mockserver.model.HttpForward.forward;
import static org.mockserver.model.HttpRequest.request;
import static org.mockserver.model.HttpResponse.response;

import org.mockserver.integration.ClientAndServer;
import org.mockserver.matchers.MatchType;
import org.mockserver.model.Header;
import org.mockserver.model.JsonBody;
import org.mockserver.model.Parameter;

/**
 * 静态的mock
 * @author Administrator
 *
 */
public class StaticMock {
	
	static final String BASEPATH_VIEW = "/view";
	static final String BASEPATH_GET = "/get";
	static final String BASEPATH_UPDATE = "/update";
	static final String BASEPATH_DELETE = "/del";
	static final String BASEPATH_FORWARD = "/forward";
	static final String BASEPATH_FIND = "/find";
	
	public static void mockForward(ClientAndServer mockServer) {
		mockServer.when(request().withMethod("GET").withPath(BASEPATH_FORWARD))
				.forward(forward().withHost("baidu.com").withPort(80)
				// HTTPS
				// .withPort(443)
				// .withScheme(Scheme.HTTPS)
				);

	}

	public static void mockResponese(ClientAndServer mockServer) {
		mockServer.when(request().withMethod("GET").withPath(BASEPATH_GET)).respond(response().withStatusCode(200)
				// .withHeader("Content-Type", "plain/text")
				.withCookie("Session", "97d43b1e-fe03-4855-926a-f448eddac32f")
				.withBody(new JsonBody("{" + System.lineSeparator() + "    \"id\": 1," + System.lineSeparator()
						+ "    \"name\": \"姓名\"," + System.lineSeparator() + "    \"price\": \"123\","
						+ System.lineSeparator() + "    \"price2\": \"121\"," + System.lineSeparator()
						+ "    \"enabled\": \"true\"," + System.lineSeparator() + "    \"tags\": [\"home\", \"green\"]"
						+ System.lineSeparator() + "}")));
	}

	public static void mockBodyPlaceholder(ClientAndServer mockServer) {
		mockServer.when(request().withBody(new JsonBody(
				"{" + System.lineSeparator() + "    \"id\": 1," + System.lineSeparator()
						+ "    \"name\": \"A_${json-unit.any-string}\"," + System.lineSeparator()
						+ "    \"price\": \"${json-unit.any-number}\"," + System.lineSeparator()
						+ "    \"price2\": \"${json-unit.ignore-element}\"," + System.lineSeparator()
						+ "    \"enabled\": \"${json-unit.any-boolean}\"," + System.lineSeparator()
						+ "    \"tags\": [\"home\", \"green\"]" + System.lineSeparator() + "}",
				MatchType.ONLY_MATCHING_FIELDS))).respond(response().withBody("some_response_body"));
	}

	public static void mockBody(ClientAndServer mockServer) {
		mockServer.when(request().withBody(
				new JsonBody("{" + System.lineSeparator() + "    \"id\": 1," + System.lineSeparator()
						+ "    \"name\": \"A green door\"," + System.lineSeparator() + "    \"price\": 12.50,"
						+ System.lineSeparator() + "    \"tags\": [\"home\", \"green\"]" + System.lineSeparator() + "}",
						MatchType.STRICT// MatchType.ONLY_MATCHING_FIELDS
				))).respond(response().withBody("some_response_body"));
	}

	public static void mockHeads(ClientAndServer mockServer) {
		mockServer
				.when(request().withMethod("GET").withPath("/musthead").withHeaders(
						new Header("Accept", "application/json"), // header("Accept.*", ".*gzip.*")
						new Header("Accept-Encoding", "gzip, deflate, br")))
				.respond(response().withBody("some_response_body"));
	}

	public static void mockGet(ClientAndServer mockServer) {
		mockServer.when(request().withMethod("GET").withPath(BASEPATH_VIEW + "/cart")
		// 设置两次后失效
		// ,Times.exactly(2)
		// 设置5秒内只能访问一次
		// ,Times.once(),
		// TimeToLive.exactly(TimeUnit.SECONDS, 5L)
		// 正则，starts with "/some"
		// .withPath("/some.*")//.withPath(not("/some.*"))
		).respond(response().withBody("some_response_body"));

		mockServer
				.when(request().withMethod("GET").withPath(BASEPATH_VIEW + "/cart1")
						.withQueryStringParameters(new Parameter("cartId", "055CA455-1DF7-45BB-8535-4F83E7266092")))
				.respond(response().withBody("some_response_body_withQueryStringParameters"));

	}

	public static void mockGetPathParameter(ClientAndServer mockServer) {

		mockServer
				.when(request().withMethod("GET").withPath(BASEPATH_VIEW + "/cart/{cartId}")
						.withPathParameters(new Parameter("cartId", "[A-Z0-9\\-]+"))
						.withQueryStringParameters(new Parameter("year", "2019"), new Parameter("month", "10"),
								new Parameter("userid", "[A-Z0-9\\\\-]+")))
				.respond(response().withBody("some_response_body"));

	}

	public static void mockPost(ClientAndServer mockServer) {

		mockServer
				.when(request().withMethod("POST").withPath(BASEPATH_VIEW)
						.withBody("{username: 'user', password: 'mypassword'}"))
				.respond(response().withStatusCode(200).withCookie("sessionId", "2By8LOhBmaW5nZXJwcmludCIlMDAzMW")
						.withHeaders(new Header("Content-Type", "application/json; charset=utf-8"),
								new Header("Cache-Control", "public, max-age=86400"))
						.withBody("{ \"apply_id\": \"000001\", \"overdued\": \"Y\" }"));
	}

}
