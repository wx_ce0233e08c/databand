import Cookies from 'js-cookie'

const state = {
  siteid: 0,
  device: 'desktop',
  size: Cookies.get('size') || 'medium'
}

const mutations = {
  TOGGLE_SITEID: (state, id) => {
    // state.siteid = id
    // Cookies.set('siteid', id)
    sessionStorage.setItem('siteid', id)
    state.siteid = sessionStorage.getItem('siteid')
  },
  TOGGLE_DEVICE: (state, device) => {
    state.device = device
  },
  SET_SIZE: (state, size) => {
    state.size = size
    Cookies.set('size', size)
  }
}

const actions = {
  toggleSiteid({ commit }, id) {
    commit('TOGGLE_SITEID', id)
  },
  toggleDevice({ commit }, device) {
    commit('TOGGLE_DEVICE', device)
  },
  setSize({ commit }, size) {
    commit('SET_SIZE', size)
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
